<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class AuthController extends Controller
{
    public function login(Request $request){
        $credentials = $request->validate([
            'email' => ['required', 'email'],
            'password' => ['required'],
        ]);

        if ($token = auth()->attempt($credentials)) {
            // $request->session()->regenerate();
 
            return response()->json([
                'status' => true,
                'info' =>'sukses',
                'data' => $token
            ]);
        }
        
        return response()->json([
            'status' => false,
            'info' =>'The provided credentials do not match our records.'
        ]);

    }
}
