<?php

namespace App\Http\Controllers;

use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;

class UserController extends Controller
{
    public function list(){
        $users = User::paginate(10);
        return response()->json([
            'status' => true,
            'info' =>'sukses',
            'data' => $users
        ]);
    }
    public function save(Request $request){
        
        $user = new User();
        $user->nama_lengkap = $request->nama_lengkap;
        $user->nama_panggilan = $request->nama_panggilan;
        $user->tempat_lahir = $request->tempat_lahir;
        $user->tanggal_lahir = $request->tanggal_lahir;
        $user->email = $request->email;
        $user->password = Hash::make($request->password);
        $user->provinsi = $request->provinsi;
        $user->kota = $request->kota;
        $user->kecamatan = $request->kecamatan;
        $user->kelurahan = $request->kelurahan;
        $user->rw = $request->rw;
        $user->rt = $request->rt;
        if($user->save()){
            return response()->json([
                'status' => true,
                'info' =>'sukses'
            ]);
        }
        return response()->json([
            'status' => false,
            'info' =>'gagal'
        ]);

    }

    public function saveAll(Request $request){
        $user = new User();
        $user->telepon = $request->telepon;
        $user->agama = $request->agama;
        $user->{'status-pernikahan'} = $request->staus_pernikahan;
        $user->no_rumah = $request->no_rumah;
        $user->alamat = $request->alamat;
        $user->pekerjaan = $request->pekerjaan;
        $user->pendidikan_terakhir = $request->pendidikan_terakhir;
        // $user->iduser_ayah = $request->iduser_ayah;
        // $user->iduser_suami = $request->iduser_suami;
        if($user->save()){
            return response()->json([
                'status' => true,
                'info' =>'sukses'
            ]);
        }
        return response()->json([
            'status' => false,
            'info' =>'gagal'
        ]);
    }
}
