<?php

namespace App\Http\Controllers;

use App\Models\Agenda;
use Illuminate\Http\Request;


class AgendaController extends Controller
{
    public function list(){
        $agenda = Agenda::paginate(10);
        return response()->json([
            'status' => true,
            'info' =>'sukses',
            'data' => $agenda
        ]);
    }

    public function save(Request $request){
        
        $agenda = new Agenda();
        $agenda->title = $request->title;
        $agenda->tanggal = $request->tanggal;
        $agenda->start = $request->start;
        $agenda->end = $request->end;
        $agenda->user_id = $request->user_id;
        $agenda->deskripsi = $request->deskripsi;
       
        if($agenda->save()){
            return response()->json([
                'status' => true,
                'info' =>'sukses'
            ]);
        }
        return response()->json([
            'status' => false,
            'info' =>'gagal'
        ]);

    }

    public function saveAll(Request $request){
        $agenda = new Agenda();
        $agenda->title = $request->title;
        $agenda->tanggal = $request->tanggal;
        $agenda->start = $request->start;
        $agenda->end = $request->end;
        $agenda->user_id = $request->user_id;
        $agenda->deskripsi = $request->deskripsi;

        if($agenda->save()){
            return response()->json([
                'status' => true,
                'info' =>'sukses'
            ]);
        }
        return response()->json([
            'status' => false,
            'info' =>'gagal'
        ]);
    }
}