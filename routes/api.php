<?php

use App\Http\Controllers\AuthController;
use App\Http\Controllers\UserController;
use App\Http\Controllers\AgendaController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});
Route::post('/auth/login',[AuthController::class,'login']);
Route::post('/user/save',[UserController::class,'save']);
Route::get('/user/list',[UserController::class,'list']);
Route::post('/agenda/save',[AgendaController::class,'save']);
Route::get('/agenda/list',[AgendaController::class,'list']);

